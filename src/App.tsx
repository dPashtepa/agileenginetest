import React, { FunctionComponent, useState, useEffect } from "react";
import { ControlPanel } from "./components/control-panel";
import { TextZone } from "./components/text-zone";
import {
  getMockText,
  applyOptions,
  applyColor,
  getSynonyms,
  replaceSelectedText
} from "./utils/text.service";
import "./style.scss";
import { ISynonym } from "./utils/requests";
import { Synonims } from "./components/synonyms/synonyms";

const App: FunctionComponent = () => {
  const [text, setText] = useState("");
  const [synonyms, setSynonym] = useState<ISynonym[] | undefined>(undefined);

  const getText = () => {
    getMockText().then(setText);
  };

  const findSynonyms = async () => {
    const result = await getSynonyms();
    setSynonym(result);
  };

  useEffect(() => {
    getText();
  }, []);

  const makeBold = () => applyOptions("bold");
  const makeItalic = () => applyOptions("italic");
  const makeUnderlined = () => applyOptions("underlined");
  const addColor = (color: string) => applyColor(color);

  return (
    <div className="App">
      <header>
        <span>Simple Text Editor</span>
      </header>
      <main>
        <ControlPanel
          onBoldClick={makeBold}
          onItalicClick={makeItalic}
          onUnderlineClick={makeUnderlined}
          onColorChange={addColor}
        />
        <TextZone text={text} onSelectChange={findSynonyms} />
        <Synonims synonims={synonyms} onReplace={replaceSelectedText} />
      </main>
    </div>
  );
};

export default App;
