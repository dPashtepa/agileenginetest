import React, { FunctionComponent, Fragment, SyntheticEvent } from "react";
import {
  removeClassFromSelected,
  SELECTED_CLASS_NAME
} from "../../utils/text.service";
import "./style.scss";

interface ITextZoneProps {
  text: string;
  onSelectChange: () => void;
}

const TextZone: FunctionComponent<ITextZoneProps> = ({
  text,
  onSelectChange
}) => {
  const selectElement = (event: SyntheticEvent) => {
    removeSelected();
    event.currentTarget.classList.add(SELECTED_CLASS_NAME);
    onSelectChange();
  };

  const removeSelected = () => {
    removeClassFromSelected(SELECTED_CLASS_NAME);
    onSelectChange();
  };
  return (
    <div id="editor" onClick={removeSelected}>
      {text.split(" ").map((el, ind) => (
        <Fragment key={ind}>
          <span onDoubleClick={selectElement}>{el}</span>{" "}
        </Fragment>
      ))}
    </div>
  );
};

export { TextZone };
