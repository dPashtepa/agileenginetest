import React, { FunctionComponent, useRef } from "react";
import "./style.scss";

interface IControlPanelProps {
  onBoldClick: () => void;
  onItalicClick: () => void;
  onUnderlineClick: () => void;
  onColorChange: (color: string) => void;
}

const ControlPanel: FunctionComponent<IControlPanelProps> = ({
  onBoldClick,
  onItalicClick,
  onUnderlineClick,
  onColorChange
}) => {
  const color = useRef<HTMLInputElement>(null);
  const changeColor = () => {
    if (color.current) onColorChange(color.current.value);
  };
  return (
    <div className="control-panel">
      <button className="format-action" type="button" onClick={onBoldClick}>
        <b>B</b>
      </button>
      <button className="format-action" type="button" onClick={onItalicClick}>
        <i>I</i>
      </button>
      <button
        className="format-action"
        type="button"
        onClick={onUnderlineClick}
      >
        <u>U</u>
      </button>
      <input type="text" name="color" ref={color} />
      <button type="submit" onClick={changeColor}>
        Add color
      </button>
    </div>
  );
};

export { ControlPanel };
