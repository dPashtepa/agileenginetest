import React, { FunctionComponent } from "react";
import "./style.scss";
import { ISynonym } from "../../utils/requests";

interface ISynonimsProps {
  synonims?: ISynonym[];
  onReplace: (word: string) => void;
}

const Synonims: FunctionComponent<ISynonimsProps> = ({
  synonims,
  onReplace
}) => {
  if (synonims && synonims.length) {
    return (
      <div className="synonims">
        <h3>Replace with synonim:</h3>
        {synonims.map((el, ind) => (
          <button
            key={ind}
            onClick={() => onReplace(el.word)}
            className="synonim"
          >
            {el.word}
          </button>
        ))}
      </div>
    );
  }
  return null;
};

export { Synonims };
