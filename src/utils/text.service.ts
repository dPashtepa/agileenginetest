import { getSynonymsRequest } from "./requests";

export const getMockText = (): Promise<string> => {
  return new Promise(function(resolve) {
    resolve(
      "A year ago I was in the audience at a gathering of designers in San Francisco. There were four designers on stage, and two of them worked for me. I was there to support them. The topic of design responsibility came up, possibly brought up by one of my designers, I honestly don’t remember the details. What I do remember is that at some point in the discussion I raised my hand and suggested, to this group of designers, that modern design problems were very complex. And we ought to need a license to solve them."
    );
  });
};

export const SELECTED_CLASS_NAME = "selected";

const getSelected = () => document.querySelector(`.${SELECTED_CLASS_NAME}`);

export const addClassToSelected = (className: string) => {
  getSelected()?.classList.add(className);
};

export const removeClassFromSelected = (className: string) => {
  getSelected()?.classList.remove(className);
};

export const applyOptions = (className: string) => {
  getSelected()?.classList.contains(className)
    ? removeClassFromSelected(className)
    : addClassToSelected(className);
};

export const applyColor = (color: string) => {
  const selected = getSelected() as HTMLElement;
  if (selected) selected.style.color = color;
};

export const getSynonyms = async () => {
  const value = getSelected()?.textContent;
  if (value) {
    const { data } = await getSynonymsRequest(value);
    return data;
  }
};

export const replaceSelectedText = (replacementText: string) => {
  const selected = getSelected();
  if (selected) {
    selected.innerHTML = replacementText;
  }
};
