import axios from "axios";

export interface ISynonym {
  word: string;
  score: number;
}

export const getSynonymsRequest = (query: string) =>
  axios.get<ISynonym[]>("https://api.datamuse.com//words?", {
    params: {
      rel_syn: query
    }
  });
