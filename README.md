# Simple text editor

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Initial setup

Run `yarn install` in order to setup application

## Development server

Run `yarn start` for a dev server.

## Notes

- Use double click for interactions
